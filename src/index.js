import Vue from 'vue';
import BodyTemperature from './BodyTemperature.vue';
import VueHighlightJS from 'vue-highlightjs'


var highmedPluginBodyTemperature = {
  install: function(Vue, options) {
    // const {
    //   store
    // } = options
    // if (!store) {
    //   throw new Error("Please provide vuex store.");
    // }

    // store.registerModule({
    //   states,
    //   mutations,
    //   actions
    // });

    // Tell Vue.js to use vue-highlightjs
    Vue.use(VueHighlightJS)

    Vue.component('highmed-plugin-body_temperature', BodyTemperature);
  }
};

export default highmedPluginBodyTemperature;