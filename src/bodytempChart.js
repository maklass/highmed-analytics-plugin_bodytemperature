import {
  Line,
  mixins
} from 'vue-chartjs'

var _mixins = mixins,
  reactiveProp = _mixins.reactiveProp;


export default {
  extends: Line,
  mixins: [reactiveProp],
  props: ['options'],
  mounted: function() {
    this.renderChart(this.chartData, this.options)
  }
}